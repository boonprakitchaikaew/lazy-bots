dc-reset-to-factory:
	- docker stop $$(docker ps -a -q)
	- docker kill $$(docker ps -q)
	- docker rm $$(docker ps -a -q)
	- docker rm $$(docker ps -a -q)
	- docker rmi $$(docker images -q)
	- docker system prune --all --force --volumes

dcup-dev:
	docker-compose up

dcup-prod:
	docker-compose -f ./docker-compose.prod.yml up

dc-down:
	docker-compose down

dc-clear:
	docker-compose down
	docker rmi -f $(docker images | grep doszy)

cert-binary-window:
	cd certs/src && go build -ldflags "-X main.Version=1.0.0" -o ../binary/mkcert_window
	
cert-binary-linux:
	cd certs/src && go build -ldflags "-X main.Version=1.0.0" -o ../binary/mkcert_linux

cert-binary-mac:
	cd certs/src && go build -ldflags "-X main.Version=1.0.0" -o ../binary/mkcert_mac

hosts:
	docker network create lazy-stats-network || echo 1
	@if [ $(shell uname) = Linux ]; then\
		./certs/binary/mkcert_linux -install;\
        ./certs/binary/mkcert_linux -ssl_path=./certs/ssl -compose=./docker-compose.yml -caddy=./Caddyfile -service=proxy lazy-stats-dev.com api.lazy-stats-dev.com;\
	elif [ "$(uname)" == "Darwin" ]; then\
		./certs/binary/mkcert_mac -install;\
		./certs/binary/mkcert_mac -ssl_path=./certs/ssl -compose=./docker-compose.yml -caddy=./Caddyfile -service=proxy lazy-stats-dev.com api.lazy-stats-dev.com;\
	else \
		./certs/binary/mkcert_window -install;\
		./certs/binary/mkcert_window -ssl_path=./certs/ssl -compose=./docker-compose.yml -caddy=./Caddyfile -service=proxy lazy-stats-dev.com api.lazy-stats-dev.com;\
	fi
	
	sudo -- sh -c "echo 127.0.0.1  lazy-stats-dev.com >> /etc/hosts"
	sudo -- sh -c "echo 127.0.0.1  api.lazy-stats-dev.com >> /etc/hosts"