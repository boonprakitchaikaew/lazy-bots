## Lazy stats

### Tech Stacks
- Nginx by Caddyfile
- Docker
- Node
- Redis

### Required tools
- Docker
- Docker compose

### Commands
#### First running on machine
```
> make hosts
```
#### Start all services on local
```
> make dcup-dev
```
then you can access `lazy-stats-dev.com` on your browser or with ip on your laptop
