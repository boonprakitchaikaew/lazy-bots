const express = require('express')
const app = express()
const redisClient = require('./redis-client')
const cors = require('cors')

app.use(cors())

app.get('/pancakesquad', async (req, res) => {
    try {
        const rawData = await redisClient.getAsync('pancakesquad')
        return res.json(JSON.parse(rawData))
    } catch (e) {
        return res.json([])
    }
})

const PORT = process.env.PORT || 3000
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
})