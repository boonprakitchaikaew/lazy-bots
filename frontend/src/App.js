import './App.css';
import LazyLoad from 'react-lazyload'
import { Sloth } from './image' 
import randomColor from 'randomcolor'
import { useEffect, useState, useCallback } from 'react'
import axios from 'axios'
import { CircularProgressbar } from 'react-circular-progressbar'
import 'react-circular-progressbar/dist/styles.css'
import FlipMove from 'react-flip-move'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from 'react-router-dom'

function App() {
    const [countdown, setCountdown] = useState(30)
    const [nfts, setnfts] = useState([])
    const [maxScore, setMaxScore] = useState(0)
    const [colors, setColors] = useState({})
    const [isLoading, setLoading] = useState(true)
    const [toggleMenu, setToggleMenu] = useState(true)

    const fetchNFTs = useCallback(
        async () => {
            const response = await axios.get(`${process.env.REACT_APP_API_BASE_URL}/pancakesquad`)
            const nfts = response.data
            const scores = nfts.map((nft) => {
                return parseFloat(nft.score)
            })
            
            const maxScore = (() => {
                const score = Math.max.apply(null, scores)
                if (score > 1000) {
                    return 1000
                }
        
                return score
            })()

            setMaxScore(maxScore)
            setnfts(nfts.sort((a, b) => parseFloat(a.currentAskPrice) - parseFloat(b.currentAskPrice)))
            setLoading(false)
        },
        [],
    )

    useEffect(() => {
        const arrays = Array.from(Array(10000).keys())

        const colors = arrays.reduce((o, n, i) => {
            return {
                ...o,
                [`${i}`]: randomColor({
                    luminosity: 'light',
                    format: 'hsla'
                })
            }
        }, {})
        
        setColors(colors)
    }, [])

    useEffect(() => {
        const timer = setInterval(() => {
            setCountdown((prevState) => {
                if (prevState - 1 < 0) {
                    fetchNFTs()
                    return 30
                }

                return prevState - 1
            })
        }, 1000)

        fetchNFTs()

        return () => {
            clearInterval(timer)
        }
    }, [])

    if (isLoading) {
        return (<div style={{ height: '100vh', width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
            <div class='zoom-in-zoom-out'>
                <Sloth width='180' height='180' />
            </div>
        </div>)
    }

    return (
        <Router>
            <div>
                <div class='counter-container'>
                    <CircularProgressbar 
                        value={(countdown * 100) / 30} 
                        text={countdown} 
                        counterClockwise 
                    />
                </div>
                <div style={{ position: 'fixed', right: 12, bottom: 120, color: 'gray', cursor: 'pointer' }}>
                    https://lazy-stats.com
                </div>
                <nav class='navbar' role='navigation' aria-label='main navigation'>
                    <div class='navbar-brand'>
                        <Sloth />
                        <a role='button' class='navbar-burger' onClick={() => setToggleMenu(!toggleMenu)}>
                            <span aria-hidden='true'></span>
                            <span aria-hidden='true'></span>
                            <span aria-hidden='true'></span>
                        </a>
                    </div>
                    <div class='navbar-menu' style={{ display: toggleMenu ? 'flex' : 'none' }}>
                        <div class='navbar-start'>
                            <Link to='/' class='navbar-item'>Home</Link>
                            <a class='navbar-item' href='https://twitter.com/boonprakit'>
                                Twitter
                            </a>
                            <Link to='/donate' class='navbar-item'>Donate</Link>
                            <Link to='/how-come' class='navbar-item'>How come</Link>
                        </div>
                    </div>
                </nav>
                <div class='stats-content'>
                    <Switch>
                        <Route path='/' exact>
                            <article class='message is-success' style={{ margin: 40 }}>
                                <div class='message-header'>
                                    <p>How to calculate the rarity score</p>
                                </div>
                                <div class='message-body'>
                                    Based on this document https://raritytools.medium.com/ranking-rarity-understanding-rarity-calculation-methods-86ceaeb9b98c
                                    <br />
                                    <br />
                                    Thank you for credit: <a href='https://web.telegram.org/z/#2008459226'>@PancakeswapNftMarketBot</a>
                                </div>
                            </article>
                        </Route>
                        <Route path='/how-come' exact>
                            <article class='message is-success how-come-container'>
                                <div class='message-header'>
                                    <p>Why I did this?</p>
                                </div>
                                <div class='message-body'>
                                    The starting point for this site is that I made it for myself and didn't want to spend a lot of time building it so it is not that good but I still wanted to share it with the pancake community.
                                </div>
                            </article>
                        </Route>
                        <Route path='/donate' exact>
                            <article class='message is-info donate-container'>
                                <div class='message-header'>
                                    <p>Inspiring my passion</p>
                                </div>
                                <div class='message-body'>
                                    This site is not that good and you would have a question like why do I need to donate. 
                                    I need to say that I do not have any PancakeSquad NFTs so this site is not my benefit at all BUT I still have to pay for servers, domain ... 
                                    BUT again, Don't worry As long as I do not lose the money for my cypto investment, 
                                    I'll be running this site for a long time. You can also request what you want to see to my twitter. I will not promise that everything can be done but I will do if I can do it.
                                    <div class='wallet'>
                                        <img width='80' height='80' src='/wallet.png' />
                                        0x212B8e59968E07217Ac7e8EC4dAe5E6FEc5D13ED
                                    </div>
                                </div>
                            </article>
                        </Route>
                    </Switch>
                    <FlipMove>
                        {
                            nfts.map((nft, i) => {
                                return (
                                    <div key={nft.tokenId} class='bar-container'>
                                        <a 
                                            target='_blank'
                                            href={`https://pancakeswap.finance/nfts/collections/0x0a8901b0e25deb55a87524f0cc164e9644020eba/${nft.tokenId}`}
                                            class='bar'
                                            style={{ 
                                                backgroundColor: colors[i], 
                                                width: `${(parseFloat(nft.score)*100)/maxScore > 100 ? 100 : (parseFloat(nft.score)*100)/maxScore}%`, 
                                            }}
                                        >
                                            <span class='bnb' style={{ fontSize: 8, marginLeft: 8 }}>{nft.currentAskPrice} BNB</span>
                                                <LazyLoad height={24} offset={100} style={{ position: 'absolute', right: 0, top: 0, bottom: 0}}>
                                                    <div class='nft-image'>
                                                        <img src={`https://static-nft.pancakeswap.com/mainnet/0x0a8901b0E25DEb55A87524f0cC164E9644020EBA/pancake-squad-${nft.tokenId}-1000.png`} style={{ height: 24, width: 24, borderRadius: '50%' }}></img>
                                                    </div>
                                                </LazyLoad>
                                            <span class='score' style={{ fontSize: 8, position: 'absolute', right: -30 }}>{nft.score.toFixed(2)}</span>
                                        </a>
                                    </div>
                                )
                            })
                        }
                    </FlipMove>
                </div>
            </div>
        </Router>
    )
}

export default App;
