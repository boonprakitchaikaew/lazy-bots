const axios = require('axios')
const _ = require('lodash')
const rarities = require('./rarity.json')
const { createClient } = require('redis')
const redisClient = require('./redis-client')

const fetchData = async () => {
    let nfts = []
    let page = 0
    while (page < 10) {
        const data = JSON.parse(`{\"query\":\"\\n        query getNftsMarketData($first: Int, $skip: Int!, $where: NFT_filter, $orderBy: NFT_orderBy, $orderDirection: OrderDirection) {\\n          nfts(where: $where, first: $first, orderBy: $orderBy, orderDirection: $orderDirection, skip: $skip) {\\n            \\n  tokenId\\n  metadataUrl\\n  currentAskPrice\\n  currentSeller\\n  latestTradedPriceInBNB\\n  tradeVolumeBNB\\n  totalTrades\\n  isTradable\\n  updatedAt\\n  otherId\\n  collection {\\n    id\\n  }\\n\\n            transactionHistory {\\n              \\n  id\\n  block\\n  timestamp\\n  askPrice\\n  netPrice\\n  withBNB\\n  buyer {\\n    id\\n  }\\n  seller {\\n    id\\n  }\\n\\n            }\\n          }\\n        }\\n      \",\"variables\":{\"where\":{\"collection\":\"0x0a8901b0e25deb55a87524f0cc164e9644020eba\",\"isTradable\":true},\"first\":1000,\"skip\":${i*1000},\"orderBy\":\"currentAskPrice\",\"orderDirection\":\"asc\"}}`)
    
        const options = {
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            data: data,
            url: 'https://api.thegraph.com/subgraphs/name/pancakeswap/nft-market'
        }
            
        const response = await axios(options)
        const newNfts = _.get(response, 'data.data.nfts', [])

        if (newNfts.length === 0) return nfts
        
        nfts = [...nfts, ...newNfts]
        page = page + 1
    }

    return nfts
}

const sleep = (time) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve()
        }, time)
    })
}

const main = async () => {
    while (true) {
        try {
            const nfts = await fetchData()
            const data = nfts.map((nft) => {
                return {
                    ...nft,
                    score: rarities[nft.tokenId]
                }
            })

            await redisClient.setAsync('pancakesquad', JSON.stringify(data))
            console.log('Saved to cache!', new Date())
        } catch (err) {
            console.log('Got err', err)
        }

        await sleep(parseInt(process.env.CAPTURE_INTERVAL || 15) * 1000)
    }
}

main()